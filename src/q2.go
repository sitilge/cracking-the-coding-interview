package main

func Q2(input1 string, input2 string) bool {
	if len(input1) != len(input2) {
		return false
	}

	var found bool

	for _, chi := range input1 {
		for _, chj := range input2 {
			if chi == chj {
				found = true
				
				break
			}
		}

		if !found {
			return false
		}
	}

	return true
}