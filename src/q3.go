package main

func Q3(input1 string, input2 int) string {
	var output string

	for _, ch := range input1 {
		if ch == ' ' {
			output += "%20"

			continue
		}

		output += string(ch)
	}

	return output
}