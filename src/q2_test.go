package main

import "testing"

func TestQ2Pass(t *testing.T) {
	inputs1 := []string{
		"abc",
		"cab",
		"1a2b",
		"",
	}

	inputs2 := []string{
		"abc",
		"bca",
		"12ab",
		"",
	}

	for i := range inputs1 {
		if !Q2(inputs1[i], inputs2[i]) {
			t.Fail()
		}
	}
}

func TestQ2Fail(t *testing.T) {
	inputs1 := []string{
		"abc",
		"",
	}

	inputs2 := []string{
		"aabc",
		"a",
	}

	for i := range inputs1 {
		if Q2(inputs1[i], inputs2[i]) {
			t.Fail()
		}
	}
}
