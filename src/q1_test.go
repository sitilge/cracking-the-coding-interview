package main

import "testing"

func TestQ1Pass(t *testing.T) {
	inputs := []string{
		"abc",
		"AbC",
		"a",
		"",
		"012a",
	}
	
	for _, input := range inputs {
		if !Q1(input) {
			t.Fail()
		}
	}
}

func TestQ1Fail(t *testing.T) {
	inputs := []string{
		"aabc",
		"123010",
		"a3bbbb",
	}

	for _, input := range inputs {
		if Q1(input) {
			t.Fail()
		}
	}
}
