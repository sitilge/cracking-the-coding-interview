package main

import (
	"strings"
)

func Q4(input string) (string, bool) {
	//Basically, we need to check if every element in the map repeats
	//at least 2*n times. Optionally, one and only one element might appear only once.

	input = strings.ToLower(input)

	m := make(map[rune]int, 0)

	for _, b := range input {
		if b == ' ' {
			continue
		}

		m[b]++
	}

	var single string

	//find which one is the single rune
	for b, n := range m {
		if n % 2 != 0 {
			if single == "" {
				single = string(b)
			} else {
				return "", false
			}
		}
	}

	//for more permutations, code here...
	var p1 string

	//TODO - maps do not produce consistent output when iterated over: https://blog.golang.org/go-maps-in-action
	for b, n := range m {
		//generate just two permutations, we don't need all of them...
		//we can go from 0 to n / 2, then add single, then reverse the first part
		for i := 0; i < n / 2; i++ {
			p1 += string(b)
		}
	}

	var p2 string

	for i := len(p1) - 1; i >= 0; i-- {
		p2 += string(p1[i])
	}

	//TODO - careful with strings - when you insert an empty string, you actually
	//TODO - increase the string length

	return p1 + single + p2, true
}