package main

import (
	"fmt"
	"testing"
)

func TestQ4Pass(t *testing.T) {
	inputs := []string {
		"Tact Coa",
		"Mama Bee",
		"Yeey",
		"",
	}

	//outputs := []string {
	//	"tacocat",
	//	"maebeam",
	//	"yeey",
	//	"",
	//}


	//testing only for length...
	outputs := []int {
		7,
		7,
		4,
		0,
	}

	for i, output := range outputs {
		perm, status := Q4(inputs[i])

		if len(perm) != output || !status {
			fmt.Println(perm, len(perm), output)
			t.Fail()
		}
	}
}

func TestQ4Fail(t *testing.T) {

}

