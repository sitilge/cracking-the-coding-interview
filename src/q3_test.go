package main

import (
	"testing"
)

func TestQ3Pass(t *testing.T) {
	inputs1 := []string {
		"Mr John Smith",
		"Mr John",
		"Mr",
		"",
		"Mr ",
	}

	inputs2 := []int {
		13,
		7,
		2,
		0,
		3,
	}

	outputs := []string {
		"Mr%20John%20Smith",
		"Mr%20John",
		"Mr",
		"",
		"Mr%20",
	}

	for i := range inputs1 {
		if outputs[i] != Q3(inputs1[i], inputs2[i]) {
			t.Fail()
		}
	}
}

func TestQ3Fail(t *testing.T) {

}
