package main

func Q1(input string) bool {
	m := make(map[int32]bool, 0)

	for _, ch := range input {
		_, ok := m[ch]
		
		if ok {
			return false
		}

		m[ch] = true
	}

	return true
}